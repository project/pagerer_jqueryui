<?php

namespace Drupal\Tests\pagerer_jqueryui\Functional;

use Drupal\Tests\pagerer\Functional\PagererTest;

/**
 * Checks Pagerer jQueryUI functionality.
 *
 * @group Pagerer
 */
class PagererJQueryUITest extends PagererTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dblog',
    'pagerer',
    'pagerer_example',
    'pagerer_jqueryui',
    'pager_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test proper functioning of multiple pagers with overridden querystring.
   */
  public function testQuerystringOverrides(): void {
    $this->markTestSkipped('Not run in Pagerer jQueryUI');
  }

  /**
   * Test proper functioning of multiple adaptive keys pagers.
   */
  public function testAdaptiveKeysQuerystring(): void {
    $this->markTestSkipped('Not run in Pagerer jQueryUI');
  }

}
